﻿namespace ObjectPools
{
    public interface IFactory<TContract>
    {
        TContract Create();
    }
}