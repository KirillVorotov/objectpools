﻿using System;
using System.Collections.Generic;

namespace ObjectPools
{
    public abstract class AbstractObjectPool<TContract> : IObjectPool
    {
        protected readonly Stack<TContract> inactiveItems = new Stack<TContract>();

        public int InactiveCount => inactiveItems.Count;
        public int ActiveCount { get; protected set; }
        public int TotalCount => InactiveCount + ActiveCount;

        public void Clear()
        {
            Resize(0);
        }

        public void ExpandBy(int numToAdd)
        {
            Resize(InactiveCount + numToAdd);
        }

        public void Put(TContract item)
        {
            if (inactiveItems.Contains(item))
            {
                throw new Exception(string.Format("Tried to return an item to pool {0} twice", GetType()));
            }
            ActiveCount--;
            inactiveItems.Push(item);
            OnPut(item);
        }

        public void Resize(int desiredCapacity)
        {
            if (InactiveCount == desiredCapacity)
            {
                return;
            }
            if (desiredCapacity < 0)
            {
                throw new Exception("Attempted to resize the pool to a negative capacity");
            }
            while (InactiveCount > desiredCapacity)
            {
                OnDestroyed(inactiveItems.Pop());
            }
            while (desiredCapacity > InactiveCount)
            {
                inactiveItems.Push(CreateNew());
            }
        }

        public void ShrinkBy(int numToRemove)
        {
            Resize(InactiveCount - numToRemove);
        }

        public void Initialize(int capacity)
        {
            if (TotalCount > 0)
            {
                return;
            }
            Resize(capacity);
        }

        protected abstract TContract CreateNew();

        protected TContract TakeInternal()
        {
            if (InactiveCount == 0)
            {
                if (TotalCount == 0)
                {
                    ExpandBy(1);
                }
                else
                {
                    ExpandBy(TotalCount);
                }
            }
            var item = inactiveItems.Pop();
            ActiveCount++;
            return item;
        }

        protected virtual void OnCreated(TContract item) { }
        protected virtual void OnDestroyed(TContract item) { }
        protected virtual void OnPut(TContract item) { }
    }
}