﻿namespace ObjectPools
{
    public interface IObjectPool
    {
        int InactiveCount { get; }
        int ActiveCount { get; }
        int TotalCount { get; }
        void Resize(int desiredCapacity);
        void ExpandBy(int numToAdd);
        void ShrinkBy(int numToRemove);
        void Clear();
        void Initialize(int capacity);
    }

    public interface IObjectPool<TContract> : IObjectPool
    {
        TContract Take();
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0> : IObjectPool
    {
        TContract Take(TParam0 param0);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6);
        void Put(TContract item);
    }

    public interface IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7> : IObjectPool
    {
        TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7);
        void Put(TContract item);
    }
}