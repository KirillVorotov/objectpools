﻿//#if UNITY_5_3_OR_NEWER
using UnityEngine;

namespace ObjectPools
{
    public class MonoObjectPool<TContract> : AbstractObjectPool<TContract>, IObjectPool<TContract>
        where TContract : MonoBehaviour
    {
        private TContract prefab;
        private Transform parentTransform;

        public MonoObjectPool(TContract prefab, Transform parentTransform = null)
        {
            this.prefab = prefab;
            this.parentTransform = parentTransform;
        }

        public TContract Take()
        {
            var item = TakeInternal();
            OnTake(item);
            return item;
        }

        protected override void OnCreated(TContract item)
        {
            item.gameObject.SetActive(false);
        }

        protected override void OnDestroyed(TContract item)
        {
            GameObject.Destroy(item.gameObject);
        }

        protected override void OnTake(TContract item)
        {
            item.gameObject.SetActive(true);
        }

        protected override void OnPut(TContract item)
        {
            item.gameObject.SetActive(false);
        }

        protected override TContract CreateNew()
        {
            var item = GameObject.Instantiate(prefab, parentTransform);
            OnCreated(item);
            return item;
        }
    }
}
//#endif