﻿namespace ObjectPools
{
    public class ObjectPool<TContract> : AbstractObjectPool<TContract>, IObjectPool<TContract>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take()
        {
            var item = TakeInternal();
            OnTake(item);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item) { }
    }

    public class ObjectPool<TContract, TParam0> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0)
        {
            var item = TakeInternal();
            OnTake(item, param0);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2, TParam3> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2, param3);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2, param3, param4);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2, param3, param4, param5);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2, param3, param4, param5, param6);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6) { }
    }

    public class ObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7> : AbstractObjectPool<TContract>, IObjectPool<TContract, TParam0, TParam1, TParam2, TParam3, TParam4, TParam5, TParam6, TParam7>
    {
        protected IFactory<TContract> factory;

        public ObjectPool(IFactory<TContract> factory)
        {
            this.factory = factory;
        }

        public TContract Take(TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7)
        {
            var item = TakeInternal();
            OnTake(item, param0, param1, param2, param3, param4, param5, param6, param7);
            return item;
        }

        protected override TContract CreateNew()
        {
            var item = factory.Create();
            OnCreated(item);
            return item;
        }

        protected virtual void OnTake(TContract item, TParam0 param0, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, TParam5 param5, TParam6 param6, TParam7 param7) { }
    }
}